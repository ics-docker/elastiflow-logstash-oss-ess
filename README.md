# centos-e3 docker image
# elastiflow-logstash-oss customized docker image (number_of_replicas = 0)

[Docker](https://www.docker.com) image based on the elastiflow logstash [image](https://github.com/robcowart/elastiflow).


Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/elastiflow-logstash-oss-ess:VERSION
```
