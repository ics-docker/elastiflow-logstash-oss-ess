FROM robcowart/elastiflow-logstash-oss:3.5.3

LABEL maintainer "alessio.curri@esss.se"

RUN sed -i s/'      "number_of_replicas": 1,'/'      "number_of_replicas": 0,'/g /etc/logstash/elastiflow/templates/elastiflow.template.json
